pub use hyper::service::service_fn_ok;
pub use hyper::{Body, Method, Request, Response, Server, StatusCode};

use std::collections::HashMap;

use crate::*;

#[allow(dead_code)]
pub enum Action<T = (), U = ()> {
    Continue(T),
    Halt(U),
}

pub type SkeletonResult = Result<hyper::Response<Body>, Box<dyn std::error::Error>>;
pub type SkeletonResponse = hyper::Response<Body>;
pub type SkeletonMiddlewareResponse = Action<hyper::Response<Body>, hyper::Response<Body>>;

pub type RouteClosureDef = Box<dyn FnMut(&mut Context) -> SkeletonResult + Send>;

#[allow(dead_code)]
pub enum Routable {
    Closure(RouteClosureDef),
    Router(Box<dyn Router + Send>),
}

pub trait Router {
    fn dispatch(&mut self, ctx: &mut Context) -> SkeletonResponse;
    fn add(&mut self, method: Method, path: &'static str, route: Routable);

    fn on_error(&mut self, route: Routable);
}

pub fn main_router() -> Box<RouterImpl> {
    Box::new(RouterImpl {
        routes: std::collections::HashMap::new(),
        on_error_route: None,
    })
}

pub struct RouterImpl {
    routes: HashMap<Method, HashMap<String, Routable>>,

    on_error_route: Option<Routable>,
}

impl Router for RouterImpl {
    fn add(&mut self, method: Method, path: &'static str, route: Routable) {
        if !self.routes.contains_key(&method) {
            self.routes.insert(method.to_owned(), HashMap::new());
        }

        let path_routes = self.routes.get_mut(&method).unwrap();
        if !path_routes.contains_key(&path.to_owned()) {
            path_routes.insert(path.to_string(), route);
        }

        return;
    }

    fn on_error(&mut self, route: Routable) {
        self.on_error_route = Some(route);

        return;
    }

    fn dispatch(&mut self, mut ctx: &mut Context) -> SkeletonResponse {
        let uri = ctx.req.uri();
        let path = uri.path().trim_end();
        let method = ctx.req.method();

        if !self.routes.contains_key(&method) {
            // Not found route:
            return Response::new(Body::from("404"));
        }

        let routes = self.routes.get_mut(&method).unwrap();
        if !routes.contains_key(&path.to_owned()) {
            // Not found route:
            return Response::new(Body::from("404"));
        }

        let route = routes.get_mut(&path.to_string()).unwrap();
        println!("BEFORE ROUTE MATCH");
        let response = match route {
            Routable::Closure(func) => func(&mut ctx),
            Routable::Router(sub_router) => Ok(sub_router.dispatch(&mut ctx))
        };
        println!("AFTER ROUTE MATCH");
        
        // println!("No point in continuing: {}", response.err().unwrap());
        std::process::exit(0);

        if response.is_err() && self.on_error_route.is_some() {
            return match &mut self.on_error_route.as_mut().unwrap() {
                Routable::Closure(func) => func(&mut ctx),
                Routable::Router(sub_router) => Ok(sub_router.dispatch(&mut ctx))
            }.unwrap();
        }

        response.unwrap()
    }
}
