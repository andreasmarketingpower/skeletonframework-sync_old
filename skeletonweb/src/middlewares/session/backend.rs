use std::collections::HashMap;

use crate::*;

pub struct MemorySessionStore {
    store: HashMap<String, HashMap<String, Vec<u8>>>,
}

impl MemorySessionStore {
    pub fn new() -> Self {
        MemorySessionStore {
            store: HashMap::new(),
        }
    }
}

impl SessionStore for MemorySessionStore {
    fn set(&mut self, session_id: String, key: String, value: Vec<u8>) {
        if let Some(map) = self.store.get_mut(&session_id) {
            map.insert(key, value);
        } else {
            let mut x = HashMap::new();
            x.insert(key, value);
            self.store.insert(session_id, x);
        }
    }

    fn get(&mut self, session_id: String, key: String) -> Vec<u8> {
        if let Some(map) = &self.store.get(&session_id) {
            if let Some(value) = map.get(&key) {
                return value.clone();
            }
        }

        vec![]
    }

    fn delete(&mut self, session_id: String, key: String) {
        if let Some(map) = self.store.get_mut(&session_id) {
            map.remove(&key);
        }
    }
}
