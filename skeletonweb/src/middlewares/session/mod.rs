use std::sync::{Arc, Mutex};

use crate::*;

pub mod backend;

pub trait SessionStore {
    fn set(&mut self, session_id: String, key: String, value: Vec<u8>);
    fn get(&mut self, session_id: String, key: String) -> Vec<u8>;
    fn delete(&mut self, session_id: String, key: String);
}

pub struct SessionWrapper {
    session_id: String,
    backend: Arc<Mutex<Box<dyn SessionStore + Send>>>,
}

impl SessionWrapper {
    pub fn set(&mut self, key: String, value: Vec<u8>) {
        self.backend
            .lock()
            .unwrap()
            .set(self.session_id.clone(), key, value);
    }

    pub fn get(&mut self, key: String) -> Vec<u8> {
        self.backend
            .lock()
            .unwrap()
            .get(self.session_id.clone(), key)
    }

    pub fn delete(&mut self, key: String) {
        self.backend
            .lock()
            .unwrap()
            .delete(self.session_id.clone(), key)
    }
}

pub struct SessionMiddleware {
    backend: Arc<Mutex<Box<dyn SessionStore + Send>>>,
}

impl SessionMiddleware {
    pub fn new(backend: Box<dyn SessionStore + Send>) -> SessionMiddleware {
        SessionMiddleware {
            backend: Arc::new(Mutex::new(backend)),
        }
    }
}

impl AroundMiddleware for SessionMiddleware {
    fn around(
        &mut self,
        mut ctx: &mut Context,
        next: &mut dyn FnMut(&mut Context) -> SkeletonMiddlewareResponse,
    ) -> SkeletonMiddlewareResponse {
        if None == ctx.cookies().get("sessionid") {
            let session_id = format!("{}", uuid::Uuid::new_v4());
            ctx.cookies().add(Cookie::new("sessionid", session_id));
        }

        let backend = Arc::clone(&mut self.backend);
        ctx.set_data("session", backend);

        let resp = next(&mut ctx);

        resp
    }
}

impl Context {
    pub fn sessions(&mut self) -> SessionWrapper {
        let session_id = self
            .cookies()
            .get("sessionid")
            .expect("sessions() ext method has no sessionid in cookies")
            .to_string();

        let backend = self
            .data
            .get_mut("session")
            .and_then(|x| x.downcast_mut())
            .expect("no session backend found (as mut)");

        SessionWrapper {
            session_id: session_id,
            backend: Arc::clone(backend),
        }
    }
}
