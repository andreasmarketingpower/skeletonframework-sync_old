use crate::*;

pub use cookie::*;

pub struct CookieMiddleware;

impl CookieMiddleware {
    pub fn new() -> CookieMiddleware {
        CookieMiddleware {}
    }
}

impl AroundMiddleware for CookieMiddleware {
    fn around(
        &mut self,
        mut ctx: &mut Context,
        next: &mut dyn FnMut(&mut Context) -> SkeletonMiddlewareResponse,
    ) -> SkeletonMiddlewareResponse {
        pub use cookie::*;

        ctx.set_data("cookies", CookieJar::new());

        if let Some(cookie_headers) = &ctx.req.headers().get(hyper::header::COOKIE) {
            let cookie_header_string = cookie_headers.to_str().unwrap().to_string();
            for chunk in cookie_header_string.split(";") {
                if let Ok(cookie) = Cookie::parse(chunk.to_owned()) {
                    let entry = ctx.get_data_mut::<CookieJar>("cookies");
                    entry.add(cookie);
                }
            }
        }

        let mut resp = next(&mut ctx);

        let entry = ctx.get_data_mut::<CookieJar>("cookies");

        for c in entry.delta() {
            let raw_cookie = c.to_string();

            if let Ok(value) = hyper::header::HeaderValue::from_bytes(raw_cookie.as_bytes()) {
                match &mut resp {
                    Action::Continue(r) | Action::Halt(r) => {
                        r.headers_mut().append("Set-Cookie", value);
                    }
                }
            }
        }

        resp
    }
}

impl Context {
    pub fn cookies(&mut self) -> &mut CookieJar {
        self.data
            .get_mut("cookies")
            .and_then(|x| x.downcast_mut())
            .expect("cookies not registered in context")
    }
}
