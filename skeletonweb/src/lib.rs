pub mod context;
pub mod middleware;
pub mod middlewares;
pub mod router;
pub mod service;
pub mod skeleton;

pub use context::*;
pub use middleware::*;
pub use middlewares::*;
pub use router::*;
pub use service::*;
pub use session::*;
pub use skeleton::*;

pub use cookies::Cookie as Cookie;
pub use hyper as hyper;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
