pub use hyper::service::service_fn_ok;
pub use hyper::{Body, Method, Request, Response, Server, StatusCode};

pub use hyper::Request as HttpRequest;
pub use hyper::Response as HttpResponse;

use futures::{future, Future};
use futures_cpupool::CpuPool;
use std::sync::{Arc, Mutex};

pub use crate::*;

pub struct Skeleton {
    pool: CpuPool,

    router: Option<Box<dyn Router + Send>>,

    pre_middlewares: Option<Vec<Box<Middleware>>>,
    around_middlewares: Option<MiddlewareChain>,
    post_middlewares: Option<Vec<Box<Middleware>>>,

    local_address: Option<std::net::SocketAddr>,
}

impl Skeleton {
    pub fn new(router: Box<dyn Router + Send>) -> Skeleton {
        Skeleton {
            pool: CpuPool::new_num_cpus(),
            router: Some(router),
            local_address: None,

            pre_middlewares: Some(vec![]),
            around_middlewares: None,
            post_middlewares: Some(vec![]),
        }
    }

    pub fn add(&mut self, method: Method, path: &'static str, route: Routable) {
        &self.router.as_mut().unwrap().add(method, path, route);
    }

    pub fn on_error(&mut self,  route: Routable) {
        &self.router.as_mut().unwrap().on_error(route);
    }

    #[allow(dead_code)]
    pub fn use_pre_middleware(&mut self, mw: Middleware) {
        self.pre_middlewares.as_mut().unwrap().push(Box::new(mw));
    }

    #[allow(dead_code)]
    pub fn use_around_middleware(&mut self, mw: Box<dyn AroundMiddleware + Send>) {
        if self.around_middlewares.is_none() {
            std::mem::replace(
                &mut self.around_middlewares,
                Some(MiddlewareChain {
                    handler: mw,
                    next: None,
                }),
            );
        } else {
            let old_handler = self.around_middlewares.take().unwrap();
            let new_handler = MiddlewareChain {
                handler: mw,
                next: Some(Box::new(old_handler)),
            };

            std::mem::replace(&mut self.around_middlewares, Some(new_handler));
        }
    }

    #[allow(dead_code)]
    pub fn use_post_middleware(&mut self, mw: Middleware) {
        self.post_middlewares.as_mut().unwrap().push(Box::new(mw));
    }

    // FIXME: Remove <Option> and make bind_and_serve self (instead of mut self)
    pub fn bind_and_serve(mut self, addr: std::net::SocketAddr) {
        let router = self.router.take().unwrap();

        let pool = Arc::new(Mutex::new(self.pool.clone()));
        
        let service = Arc::new(Mutex::new(SkeletonService {
            router: router,
            pool: Arc::clone(&pool),
            pre_middlewares: self.pre_middlewares.take().unwrap(),
            around_middlewares: self.around_middlewares.take(),
            post_middlewares: self.post_middlewares.take().unwrap(),
        }));

        let cloned_service = Arc::clone(&service);
        let cloned_pool = Arc::clone(&pool);

        let new_svc = move || {
            let cloned_service = Arc::clone(&cloned_service);
            let cloned_pool = Arc::clone(&cloned_pool);
            hyper::service::service_fn(move |req| {
                let cloned_service = Arc::clone(&cloned_service);
                let cloned_pool = Arc::clone(&cloned_pool);

                let guarded_pool = cloned_pool.lock().unwrap();
                Box::new(guarded_pool.spawn_fn(move || {
                    future::ok::<_, hyper::Error>(cloned_service.lock().unwrap().dispatch(req))
                }))
            })
        };


        let server = Server::bind(&addr)
            .serve(new_svc)
            .map_err(|e| eprintln!("server error: {}", e));

        hyper::rt::run(server);

        /*
        let addr: std::net::SocketAddr = addr.to_socket_addrs().unwrap().next().unwrap();
        self.local_address = Some(addr);

        let server = Server::bind(&addr)
            .serve(self)
            .map_err(|e| eprintln!("server error: {}", e));

        hyper::rt::run(server);
        */
    }
}

/*
impl NewService for Skeleton {
    type ReqBody = hyper::body::Body;
    type ResBody = hyper::body::Body;
    type Error = Error;
    type Service = SkeletonService;
    type InitError = Error;
    type Future = future::FutureResult<Self::Service, Self::InitError>;

    fn new_service(&self) -> Self::Future {
        future::ok(SkeletonService {
            router: *self.router.unwrap().lock().unwrap(),
            pool: self.pool.clone(),
            pre_middlewares: *self.pre_middlewares.unwrap(),
            around_middlewares: Some(*self.around_middlewares.unwrap().lock().unwrap()),
            post_middlewares: *self.post_middlewares.unwrap(),
        })
    }
}
*/

/*
impl Service for SkeletonService {
    type ReqBody = hyper::body::Body;
    type ResBody = hyper::body::Body;
    type Error = Error;
    type Future = Box<dyn Future<Item = HttpResponse<Self::ResBody>, Error = Self::Error> + Send>;

    fn call(&mut self, req: HttpRequest<Self::ReqBody>) -> Self::Future {
        Box::new(self.pool.spawn_fn(move || {
            let mut http_res = HttpResponse::<Body>::new(Body::empty());
            *http_res.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;

            handler
                .handle(&mut req)
                .unwrap_or_else(|e| {
                    println!("Error handling:\n{:?}\nError was: {:?}", req, e.error);
                    e.response
                })
                .write_back(&mut http_res, req.method());

            match Request::from_http(req, addr, &proto) {
                Ok(mut req) => {
                    // Dispatch the request, write the response back to http_res
                    handler
                        .handle(&mut req)
                        .unwrap_or_else(|e| {
                            error!("Error handling:\n{:?}\nError was: {:?}", req, e.error);
                            e.response
                        })
                        .write_back(&mut http_res, req.method)
                }
                Err(e) => {
                    println!("Error creating request:\n    {}", e);
                }
            };

            future::ok(http_res)
        }))
    }
}
*/

#[macro_export]
macro_rules! controller {
    ($struct:expr) => {
        std::sync::Arc::new(std::sync::Mutex::new($struct));
    };
}

#[macro_export]
macro_rules! add_route_for_controller {
    ($router:ident, $http_method:ident $path:tt => $struct:ident.$method:ident) => {
        let cloned = std::sync::Arc::clone(&$struct);

        $router.add(
            Method::$http_method,
            $path,
            Routable::Closure(Box::new(move |ctx| {
                cloned.lock().unwrap().$method(ctx)

                /*
                let resp = cloned.lock().unwrap().$method(ctx);
                if let Err(err) = resp {
                    let mut resp_builder = hyper::Response::builder();
                    resp_builder.status(StatusCode::INTERNAL_SERVER_ERROR);

                    return resp_builder.body(Body::from(format!("ERROR: {}", err))).unwrap();
                }

                return resp.unwrap();
                */
            })),
        );
    };
}

#[macro_export]
macro_rules! add_error_route_for_controller {
    ($router:ident, $struct:ident.$method:ident) => {
        let cloned = std::sync::Arc::clone(&$struct);
        $router.on_error(
            Routable::Closure(Box::new(move |ctx| {
                cloned.lock().unwrap().$method(ctx)
            })),
        );
    };
}