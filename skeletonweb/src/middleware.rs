use std::sync::{Arc, Mutex};

use crate::*;

pub type Middleware = Box<dyn FnMut(&mut Context) -> SkeletonMiddlewareResponse + Send>;

pub struct MiddlewareChain {
    pub handler: Box<dyn AroundMiddleware + Send>,
    pub next: Option<Box<MiddlewareChain>>,
}

impl MiddlewareChain {
    pub fn call(
        &mut self,
        mut context: &mut Context,
        router: &mut Arc<Mutex<&mut Box<dyn Router + Send>>>,
    ) -> SkeletonMiddlewareResponse {
        let mut ref_router = Arc::clone(router);

        match self.next.as_mut() {
            Some(next) => {
                return self.handler.around(&mut context, &mut |mut ctx| {
                    next.as_mut().call(&mut ctx, &mut ref_router)
                });
            }
            None => {
                return self.handler.around(&mut context, &mut |mut ctx| {
                    let resp = ref_router.lock().unwrap().dispatch(&mut ctx);
                    Action::Continue(resp)
                });
            }
        }
    }
}

pub trait AroundMiddleware {
    fn around(
        &mut self,
        ctx: &mut Context,
        next: &mut dyn FnMut(&mut Context) -> SkeletonMiddlewareResponse,
    ) -> SkeletonMiddlewareResponse;
}
