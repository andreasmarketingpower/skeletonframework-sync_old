pub use hyper::{Body, Method, Request, Response, Server, StatusCode};
use futures::stream::Stream;

use crate::*;

pub struct Context {
    pub req: SkeletonRequest,
    pub resp: Option<SkeletonResponse>,

    pub data: std::collections::HashMap<String, Box<dyn std::any::Any>>,
}

pub struct SkeletonRequest {
    body: Option<hyper::body::Body>,

    parts: http::request::Parts,
}

impl SkeletonRequest {
    pub fn from_hyper_request(req: Request<Body>) -> SkeletonRequest {
        let (parts, body) = req.into_parts();

        SkeletonRequest {
            body: Some(body),
            parts: parts,
        }
    }

    pub fn uri(&self) -> http::uri::Uri {
        self.parts.uri.clone() 
    }

    pub fn headers(&self) -> http::header::HeaderMap {
        self.parts.headers.clone()
    }

    pub fn headers_mut(&mut self) -> &mut http::header::HeaderMap {
        &mut self.parts.headers
    }

    pub fn headers_ref(&mut self) -> &http::header::HeaderMap {
        &self.parts.headers
    }

    pub fn method(&self) -> http::method::Method {
        self.parts.method.clone()
    }

    pub fn extension(&mut self) -> &mut http::Extensions {
        return &mut self.parts.extensions;
    }

    pub fn extension_ref(&self) -> &http::Extensions {
        return &self.parts.extensions;
    }

    /// consumes the body and returns the body as bytes (Vec<u8>)
    pub fn body_as_bytes(&mut self) -> Result<Vec<u8>, hyper::error::Error> {
        let body = self.body.take().expect("consuming the body").wait().fold(Ok(Vec::new()), |r, input| {
            if let Ok(mut v) = r {
                input.map(move |next_body_chunk| {
                    v.extend_from_slice(&next_body_chunk);
                    v
                })
            } else {
                r
            }
        });

        return body;
    }

    /// returns a copy of the body as bytes (Vec<u8>)
    pub fn body_as_bytes_copy(&mut self) -> Result<Vec<u8>, hyper::error::Error> {
        let body_bytes = self.body_as_bytes();
        if body_bytes.is_err() {
            return body_bytes
        }

        let cloned_body = body_bytes.unwrap();
        self.body = Some(Body::from(cloned_body.clone()));
        Ok(cloned_body)
    }

    /// consumes the body and returns hyper::body::Body
    pub fn body(&mut self) -> Option<hyper::body::Body> {
        self.body.take()
    }

    pub fn form(&mut self) -> std::collections::HashMap<String, String> {
        let bytes = self.body_as_bytes_copy().expect("copying body content for form parsing");

        let v = url::form_urlencoded::parse(&bytes);
        v.into_owned().collect()
    }

    pub fn query(&self) -> std::collections::HashMap<String, String> {
        use url::Url;

        let req = self.uri();

        let url_with_base = format!("http://localhost/{}", &req.to_string());
        let parsed = Url::parse(&url_with_base);
        if parsed.is_err() {
            // println!("ERROR: {} -> {}", &req.to_string(), parsed.err().unwrap());
            std::collections::HashMap::<String,String>::new()
        } else {
            parsed.unwrap().query_pairs().into_owned().collect()
        }
    }
}

impl Context {
    pub(crate) fn take_response(&mut self, resp: SkeletonResponse) {
        std::mem::replace(&mut self.resp, Some(resp));
    }

    pub(crate) fn release_response(&mut self) -> SkeletonResponse {
        self.resp.take().unwrap()
    }

    pub fn get_data_mut<T: 'static>(&mut self, key: &'static str) -> &mut T {
        let entry = self.data.get_mut(key);
        let casted_data: &mut dyn std::any::Any = entry.expect("key not found").as_mut();
        let obj = casted_data
            .downcast_mut::<T>()
            .expect("downcast_mut has failed");
        obj
    }

    pub fn get_data<T: 'static>(&self, key: &'static str) -> &T {
        let entry = self.data.get(key).expect("key not found");
        entry.downcast_ref::<T>().expect("downcast has failed")
    }

    pub fn set_data<T: 'static>(&mut self, key: &'static str, obj: T) {
        self.data.insert(String::from(key), Box::new(obj));
    }
}
