use std::sync::{Arc, Mutex};

use futures_cpupool::CpuPool;

use hyper;
// use hyper::service::{NewService, Service};
pub use hyper::{Body, Error, Method, Request, Response, Server, StatusCode};

use crate::skeleton::*;

pub struct SkeletonService {
    pub pool: Arc<Mutex<CpuPool>>,
    pub router: Box<dyn Router + Send + 'static>,

    pub pre_middlewares: Vec<Box<Middleware>>,
    pub around_middlewares: Option<MiddlewareChain>,
    pub post_middlewares: Vec<Box<Middleware>>,
}

impl SkeletonService {
    pub fn dispatch(&mut self, req: Request<Body>) -> SkeletonResponse {
        let mut context = Context {
            req: SkeletonRequest::from_hyper_request(req),
            resp: None,
            data: std::collections::HashMap::new(),
        };

        // pre middlewares run before each request and decide whether to continue or not.
        for middleware in &mut self.pre_middlewares {
            let func = &mut **middleware;

            let resp = func(&mut context);

            if let Action::Halt(r) = resp {
                return r;
            }

            if let Action::Continue(r) = resp {
                context.take_response(r);
            }
        }

        if let Some(middleware) = &mut self.around_middlewares {
            let mut cloned_router = Arc::new(Mutex::new(&mut self.router));

            let res = middleware.call(&mut context, &mut cloned_router);

            match res {
                Action::Continue(r) | Action::Halt(r) => context.take_response(r),
            }
        } else {
            let res = self.router.dispatch(&mut context);
            context.take_response(res);
        }

        // post middlewares run after each request and decide whether to continue or not.
        for middleware in &mut self.post_middlewares {
            let func = &mut **middleware;

            let resp = func(&mut context);

            if let Action::Halt(r) = resp {
                return r;
            }

            if let Action::Continue(r) = resp {
                context.take_response(r);
            }
        }

        let res = context.release_response();
        res
    }
}
