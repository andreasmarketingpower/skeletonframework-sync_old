use skeletonweb::*;

use skeletonweb::middlewares::cookies::*;
use skeletonweb::middlewares::session::backend as session_backend;

pub struct Pinger {
    counter: i32,
}

impl Pinger {
    pub fn pong(&mut self, ctx: &mut Context) -> SkeletonResult {
        let url = ctx.req.uri();

        self.counter += 1;

        ctx.cookies().add(Cookie::new("counter", format!("{}", self.counter)));
        ctx.cookies().add(Cookie::new("counter2", format!("{}", self.counter)));
        ctx.cookies().add(Cookie::new("whatever", format!("{}", self.counter)));

        ctx.sessions().set(
            "current_counter".to_string(),
            format!("{}", self.counter).to_owned().as_bytes().to_vec(),
        );

        if url.path_and_query()
            .unwrap()
            .query()
            .unwrap_or_default()
            .ends_with("counter")
        {
            ctx.cookies().remove(Cookie::new("counter", ""));
        }

        if url.path_and_query()
            .unwrap()
            .query()
            .unwrap_or_default()
            .ends_with("counter2")
        {
            ctx.cookies().remove(Cookie::new("counter2", ""));
        }

        if  url
            .path_and_query()
            .unwrap()
            .query()
            .unwrap_or_default()
            .ends_with("whatever")
        {
            ctx.cookies().remove(Cookie::new("whatever", ""));
        }

        // *resp.body_mut() = Body::from("foo");
        // *resp.status_mut() = StatusCode::BAD_REQUEST;

        let uri = ctx.req.uri();
        let path = uri.path();

        let resp = Response::new(Body::from(format!(
            "You called {} already {} times",
            path, self.counter
        )));

        Ok(resp)
    }

    pub fn dec(&mut self, ctx: &mut Context) -> SkeletonResult {
        self.counter -= 1;

        let uri = ctx.req.uri();
        let path = uri.path();

        Ok(Response::new(Body::from(format!(
            "You called {} already {} times",
            path, self.counter
        ))))
    }

    pub fn stats(&mut self, ctx: &mut Context) -> SkeletonResult {
        let counter_from_session =
            String::from_utf8(ctx.sessions().get("current_counter".to_string()))
                .expect("Failed to from_utf8");

        Ok(Response::new(Body::from(format!(
            "Requests: {} (from session: {})",
            self.counter, counter_from_session
        ))))
    }

    pub fn body(&mut self, ctx: &mut Context) -> SkeletonResult {
        let contents = ctx.req.form();
        let mut response = String::new();

        for (k, v) in contents {
            let fmtted= format!("{} => {}\n", k, v);
            response.push_str(&fmtted);
        }

        Ok(Response::new(Body::from(format!(
            "Body: {}", response
        ))))
    }

    pub fn assets(&mut self, ctx: &mut Context) -> SkeletonResult {

        let query = ctx.req.query();

        let server = AssetsServer { base_path: "./assets/".to_string() };

        println!("BEFORE FILENAME");
        let filename = query.get("file").ok_or("shit happened")?.to_string();
        println!("BEFORE SERVE");
        let serve_reply = server.serve(filename).unwrap_or(vec![]);
        println!("AFTER SERVE");
        let body = Body::from(serve_reply);
        println!("AFTER BODY FROM");
        Ok(Response::new(body))

        /*
        use std::io::prelude::*;

        let mut query = ctx.req.query();
        let file = query.get("file").unwrap();
        let path = "./assets/XXXX".to_string() + file;

        let mut f = std::fs::File::open(path)?;
        let mut buffer = Vec::new();
        f.read_to_end(&mut buffer).unwrap();

        Ok(Response::new(Body::from(buffer)))
        */
    }

    pub fn on_error(&mut self, ctx: &mut Context) -> SkeletonResult {
        Ok(Response::new(Body::from("Some bad shit happened")))
    }
}

struct AssetsServer {
    base_path: String
}

impl AssetsServer {
    pub fn new(base_path: String) -> AssetsServer {
        AssetsServer {
            base_path
        }
    }

    pub fn serve(&self, mut path: String) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        use std::io::prelude::*;

        if path.trim() == "" {
            path = "index.html".to_string();
        }

        let new_path = self.base_path.clone() + &path;
        if !self.is_safe_path(&path) {
            let err_msg = string_error::new_err(&format!("{} not safe", path));
            return Err(err_msg);
        }

        let mut f = std::fs::File::open(&new_path).expect(&format!("Failed to open {}", &new_path));

        let mut buffer = Vec::new();
        f.read_to_end(&mut buffer)?;

        Ok(buffer)
    }

    fn is_safe_path(&self, s: &str) -> bool {
        let parts = s.split('/');
        
        let mut num_segments = 0;
        
        for s in parts {
            if s == ".." {
                num_segments -= 1;
                
                if num_segments < 0 {
                    return false;
                }
                continue;
            }
        
            num_segments += 1;
        }
        
        true
    }
}


fn main() {
    let pinger = controller!(Pinger { counter: 0 });

    let mut skel = Skeleton::new(main_router());
    skel.use_around_middleware(Box::new(SessionMiddleware::new(Box::new(
        session_backend::MemorySessionStore::new(),
    ))));
    skel.use_around_middleware(Box::new(CookieMiddleware::new()));

    add_route_for_controller!(skel, GET "/pong" => pinger.pong);
    add_route_for_controller!(skel, GET "/dec" => pinger.dec);
    add_route_for_controller!(skel, GET "/stats" => pinger.stats);
    add_route_for_controller!(skel, POST "/body" => pinger.body);
    add_route_for_controller!(skel, GET "/assets" => pinger.assets);
    add_error_route_for_controller!(skel, pinger.on_error);

    skel.bind_and_serve(([127, 0, 0, 1], 8080).into());
}